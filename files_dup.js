var files_dup =
[
    [ "bno055.py", "bno055_8py.html", "bno055_8py" ],
    [ "Encoder_Driver.py", "Encoder__Driver_8py.html", "Encoder__Driver_8py" ],
    [ "Lab01.py", "Lab01_8py.html", "Lab01_8py" ],
    [ "Lab02.py", "Lab02_8py.html", "Lab02_8py" ],
    [ "Lab03_main.py", "Lab03__main_8py.html", "Lab03__main_8py" ],
    [ "Lab03_UI_front.py", "Lab03__UI__front_8py.html", "Lab03__UI__front_8py" ],
    [ "Lab04_main.py", "Lab04__main_8py.html", "Lab04__main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "P_Controller.py", "P__Controller_8py.html", [
      [ "P_Controller", "classP__Controller_1_1P__Controller.html", "classP__Controller_1_1P__Controller" ]
    ] ],
    [ "PB_Controller.py", "PB__Controller_8py.html", [
      [ "PB_Controller", "classPB__Controller_1_1PB__Controller.html", "classPB__Controller_1_1PB__Controller" ]
    ] ],
    [ "PB_FSM.py", "PB__FSM_8py.html", "PB__FSM_8py" ],
    [ "Touch_Panel_Driver.py", "Touch__Panel__Driver_8py.html", "Touch__Panel__Driver_8py" ],
    [ "TouchDriver.py", "TouchDriver_8py.html", "TouchDriver_8py" ]
];